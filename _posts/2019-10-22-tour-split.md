---
layout: post
title: "Tour Split"
date: 2019-10-22
artist: "Scree Fucking Junk & Jehova's Fitness"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=408193957/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/toursplit_cover_375px-min.jpg
bandcamp: https://thehouseorgan.bandcamp.com/album/tour-split
resonate:
archive:
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC-BY
download: https://drive.google.com/drive/folders/1sUJmIrtaBNpGmjZUQYYBBaVSr1cdx1zH
---

To celebrate the #screefuckingjehovasfitnesstourjunk we're taking a bunch of 7" lathes (cut by the excellent Loose Hair Lathe Cuts [loosehair.bandcamp.com](https://loosehair.bandcamp.com)) on the road with us.
* 24/10 - Geyger, Berlin
* 26/10 - die Schute, Hamburg
* 28/10 - 5e, Copenhagen
* 29/10 - Amok, Aarhus
* 30/10 - Maskinen, Svendborg
* 31/10 - MusicMakerSpace, Copenhagen

Scree Fucking Junk - Taking their name from a film by Jack Wormell which explores a piece of wasteland under a system of flyovers in greater London, the group’s sound, like the film, stems from a fascination with waste. Wreckage of the everyday. An examination of the textural aspects of large scale constructions. An augmentation of lo-fi aesthetics to the point of sumptuousness. The result: A mess of volatile vibrations; toxic drones, agitated noise and derelict poetry. Oppressive yet ornamental.

Jahovah's Fitness - jehova’s fitness is a noise rock band from berlin.
