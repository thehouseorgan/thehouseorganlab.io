---
layout: post
title: "Dissolution Matrix in Afterthought of Skies"
date: 2018-01-01
artist: "Murray Royston-Ward"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=1955892204/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/dissolution_matrix_cover_375px.jpeg
bandcamp: https://thehouseorgan.bandcamp.com/album/dissolution-matrix-in-afterthought-of-skies
resonate: https://beta.stream.resonate.coop/artist/6490/release/dissolution-matrix-in-afterthought-of-skies
archive: https://archive.org/details/MurrayRoystonWardDissolutionMatrixInAfterthoughtOfSkies
spotify: https://open.spotify.com/album/3zTwARAtIkOKKTO34QZVDD
apple: https://music.apple.com/gb/album/dissolution-matrix-in-afterthought-of-skies-ep/1538157892
tidal: https://tidal.com/browse/album/160549407
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC BY 3.0
download: https://drive.google.com/drive/folders/1ywnmjHbF7Oklu9SmksPqozf1A4Jk4IiE
---

This is a mixture of improvised recordings, loose compositions and archival bits and bobs. Snippets of interviews; background noises from gigs and travels; awkward moments from the cutting room floor; twigs rattled in a guitar; shuffling things around rooms; recording ruined by wind; and a never finished 'pop' song.

I've been sitting on it for some time now.

Track 1:
Additional vox: Holly Royston-Ward

Track 2:
Tempelhof wind unintentionally recorded with the assistance of Mathew Johnson (who also let me use the guitar I'm sticking bits of twigs and leaves in and he's still picking them out to this day so mega-thanx)

Track 4:
Additional guitar: Mathew Johnson
Ugandan Harp: Little Bosco

Track 5:
Partner: Holly Royston-Ward
Interviewee: Michael McCann

The work is also privately published in an edition of 25 (see [https://mroystonward.bandcamp.com/album/dissolution-matrix-in-afterthought-of-skies](https://mroystonward.bandcamp.com/album/dissolution-matrix-in-afterthought-of-skies).

This is a free/donation digital edition intended as archive and alternate distribution.
