---
layout: post
title: "Palace der Republik"
date: 2019-11-08
artist: "Benjamin Whitehill & Murray Royston-Ward"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=3033189020/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/palacederrepublik_cover_375px-min.jpg
bandcamp: https://thehouseorgan.bandcamp.com/album/palace-der-republik
archive:
resonate: https://beta.stream.resonate.coop/artist/11128/release/palace-der-republik
spotify: https://open.spotify.com/album/0HVgii6RybmSr8sAbx9oZj
tidal: https://tidal.com/browse/album/121076988
apple: https://music.apple.com/gb/album/palace-der-republik/1484993393
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC-BY
download: https://drive.google.com/drive/folders/1ywDq3Xxvn6K9D_CKTo-WBk5xaXS6X6yX
---

Ben and Murray met in Berlin in the Summer of 2018 and discovered a mutual interest in using vibration speakers to create physical feedback paths through their respective instruments. Ben utilises a range of extended guitar techniques, vibration speakers, fx pedals, and a DIY built amplifier which incorporates radio interference. Murray makes feedback systems through junk objects, architectural features such as windows, balloons, and sellotape using an array of contact mic's, transference through proximity, audio exciters, and vibration speakers. A selection of audio electronics introduce dynamic re-routing of signal paths, stray radio signals, solenoids, motors, and oscillators.

Recorded my Mathew Johnson ([www.soundengineer-berlin.com](https://www.soundengineer-berlin.com)) at Funkhaus Berlin in the summer of 2018

Mixed and Mastered by B Whitehill and M Royston-Ward
