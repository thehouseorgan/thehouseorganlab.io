---
layout: post
title: "Waterside Quests"
date: 2018-04-03
artist: "The Sons of David Ginola"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=2403393501/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/waterside_quests_cover_375px.jpeg
bandcamp: https://thehouseorgan.bandcamp.com/album/waterside-quests
resonate: https://beta.stream.resonate.coop/artist/6491/release/waterside-quests
archive: https://archive.org/details/TheSonsOfDavidGinolaWatersideQuests
spotify: https://open.spotify.com/album/5jvlBCll4ISibYqsoMAgCG
tidal: https://tidal.com/browse/album/110681236
apple: https://music.apple.com/gb/album/waterside-quests/1468292809
cc-link: https://creativecommons.org/publicdomain/zero/1.0/
cc: CC0
download: https://drive.google.com/drive/folders/1w5krhm6lPDkm9a8xOQrLWGP2QLg9YEJO
---
In 2016 Murray Royston-Ward and Kevin Sanders started started meeting to discuss internet privacy and crypto-friendly publishing.

They also both share overlapping histories through improvised musicking, running DIY record labels and the community perhaps best referred to as the No-Audience Underground.

It is natural that this should lead to recording together: using a range of digital and analog synthesis/processing techniques; so-called ‘extended’ instrument approaches; DIY circuitry; tape loops; etc.

Recording as ‘The Sons of David Ginola’, these documents are taken from improvisations made over the past 18 months in South Norwood.
