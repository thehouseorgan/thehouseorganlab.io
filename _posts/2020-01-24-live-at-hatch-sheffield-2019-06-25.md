---
layout: post
title: "Live at Hatch, Sheffield, 2019/06/25"
date: 2020-01-24
artist: "Murray Royston-Ward"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=444921635/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/live-at-hatch-sheffield-2019-06-25_375px-min.jpg
bandcamp: https://thehouseorgan.bandcamp.com/album/live-at-hatch-sheffield-2019-06-25
archive:
resonate: https://beta.stream.resonate.coop/artist/6490/release/live-at-hatch-sheffield-2019-06-25
spotify: https://open.spotify.com/album/6o61iWRxtdrrU7TPEV16rW
tidal: https://tidal.com/browse/album/132621355
apple: https://music.apple.com/gb/album/live-at-hatch-sheffield-2019-06-25-live-ep/1500553741
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC-BY
download: https://drive.google.com/drive/folders/1z5mqrq1wmrPLwcwwCmJmeGx2PLjldem6
---

Live recording from 25th June 2019 at Hatch in Sheffield.

Using contact mic's, vibration transducers, a solenoid, and various electronics, sounds are routed through two drums, a guitar, megaphone and the PA to create various physical feedback paths through objects and space. Improvisation which follows various material flows.

Thanks to RekLaw@Hatch for the live recording and Eimear for arranging the show and asking me to play.

Edits and mastering were done at home by Murray Royston-Ward.
