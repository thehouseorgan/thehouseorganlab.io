---
layout: post
title: "Always on the Move, Always Going Somewhere, Though Where This Will Be Is Not Entirely Pre­dictable"
date: 2021-07-14
artist: "Murray Royston-Ward"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=2772598068/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/alwaysonthemove_cover_375px-min.jpg
bandcamp: https://thehouseorgan.bandcamp.com/album/always-on-the-move-always-going-somewhere-though-where-this-will-be-is-not-entirely-pre-dictable
archive:
resonate: https://beta.stream.resonate.coop/artist/6490/release/always-on-the-move-always-going-somewhere-though-where-this-will-be-is-not-entirely-predictable
spotify: https://open.spotify.com/album/2ueGU6L2xGHPmYsZayjlOE
tidal: https://listen.tidal.com/album/194332742
apple: https://music.apple.com/us/album/always-on-the-move-always-going-somewhere-though/1581430583
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC-BY
download: https://drive.google.com/drive/folders/1yrAn-HWgMqQZAeIkr0_zpZSQTVSBfDLU
---

I began exploring several preamplifier circuits for use in feedback configurations with piezo contact microphones and miniature vibration speakers. Along the way, a different circuit, used to drive the vibration speaker elements, misbehaved by producing an unwelcome buzzing squeal. This could be masked by performance, the input sound over-riding the noise. These recordings are a result of on-the-spot strategies to mitigate, accompany, collaborate with, and follow its squealing. All sounds are from surfaces and materials projecting acoustically into the room, simply captured with a pair of overhead mics.

-------------------------------------------------------------

During Autumn 2020, I built a selection of preamplifier circuits primarily suited for use with piezo contact microphones. Piezo’s are noted for their tinny/poor performance and impedance mismatch is cited as causing a high pass filtering effect. I wanted to introduce preamplifier circuits into my feedback setup and see if/what difference it made. Primarily, I hoped that I might explore deeper registers of feedback whilst improving the volume of miniature equipment.

Vibration speaker elements and contact microphones are used to create feedback through objects and surfaces. In order to drive the vibration speaker elements, a power amplifier is also required. I have been using simple LM386 circuits based upon the schematics provided within the chip’s datasheet. The LM386 simultaneously embodies all that is good and all that is frustrating with DIY circuits: it is simple, cheap, easy to source, and performs admirably; whilst simultaneously, is never quite good enough, is prone to radio interference or other oscillations, and has a habit of misbehaving.

Whilst these recordings were intended to explore preamplifiers, instead, they were driven by LM386 misbehaviour. Without going too far into the details, the way the circuit is constructed plays a part. On a conventional printed circuit board things work fine but, on prototype board—a material prone to stray capacitance—this same circuit produces a kind of crackling squeal. This squeal can only be heard when the input is quiet and so, overcoming the squeal becomes a driving factor in performance.

These recordings are taken from three improvised performances, all using a variety of toms and junk percussion to hand. For the first two, I selected pairs of preamplifier circuits wired through an A/B switching box. At various junctures, I would switch between different preamplifiers to explore any differences. The frequency might shift a little, small changes in gain structure might alter timbral qualities, feedback might fail completely. Usually, I would let this play out in aleatoric and generative moments of self-playing feedback, seeing how the liveliness of these systems might alter with different circuits. In practice, the misbehaving LM386 required heuristic strategies to mitigate, accompany, collaborate with, and follow its squealing. For the third performance, I used two sets of feedback circuits simultaneously, a fluster of arms and movement trying to play four things at once whilst still working with/against the LM386 squeal.

These circuits backed me into a corner; I had to scramble my way through the available paths. As a result, these recordings are frequently busy and restless. Although I am using amplifier circuits to drive feedback, all sounds were generated acoustically. Surfaces and materials resonate and project sound into space, and this was simply captured with a pair of overhead mics.

The preamplifier circuits were: Nicholas Collins’ CD4049 preamplifier, the Tillman preamp, Richard Mudhar’s FET preamplifier, James Hawes’ FET preamplifier, and Richard Mudhar’s NE5534 preamplifier.

All track titles are drawn from Jane Bennet’s ‘Vibrant Matter’.
