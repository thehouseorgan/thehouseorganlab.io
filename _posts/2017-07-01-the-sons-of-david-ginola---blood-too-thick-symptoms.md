---
layout: post
title: "Blood Too Thick Symptoms"
date: 2017-07-01
artist: "The Sons of David Ginola"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=1109379529/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/blood_too_thick_cover_375px.jpeg
bandcamp: https://thehouseorgan.bandcamp.com/album/blood-too-thick-symptoms
resonate: https://beta.stream.resonate.coop/artist/6491/release/blood-too-thick-symptoms
archive: https://archive.org/details/TheSonsOfDavidGinolaBloodTooThickSymptoms
spotify: https://open.spotify.com/album/6fbooHIiadwfsEGD8gKGpF
tidal: https://tidal.com/browse/album/115482101
apple: https://music.apple.com/gb/album/blood-too-thick-symptoms-ep/1479403589
cc-link: https://creativecommons.org/publicdomain/zero/1.0/
cc: CC0
download: https://drive.google.com/drive/folders/1wmaV4vdgjzXD28axIpLVYaL6H7RWTpBb
---
In 2016 Murray Royston-Ward and Kevin Sanders started started meeting to discuss internet privacy and crypto-friendly publishing.

They also both share overlapping histories through improvised musicking, running DIY record labels and the community perhaps best referred to as the No-Audience Underground.

It is natural that this should lead to recording together: using a range of digital and analog synthesis/processing techniques; so-called 'extended' instrument approaches; DIY circuitry; tape loops; etc.

Recording as 'The Sons of David Ginola', this is the first release from a series of recordings made between late 2016 and early 2017.

It is released without copyright (CC0) http://creativecommons.org/publicdomain/zero/1.0/ and with a corresponding 3" cdr edition of 50.
