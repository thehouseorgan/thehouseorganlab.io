---
layout: post
title: "My Neighbor Who Lives in the City of Mirrors near My House"
date: 2016-09-02
artist: "Murray Royston-Ward"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=1674941883/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/my_neighbor_cover_375px.jpeg
bandcamp: https://thehouseorgan.bandcamp.com/album/my-neighbor-who-lives-in-the-city-of-mirrors-near-my-house
resonate: https://beta.stream.resonate.coop/artist/6490/release/my-neighbor-who-lives-in-the-city-of-mirrors-near-my-house
archive: https://archive.org/details/MurrayRoystonWardMyNeighborWhoLivesInTheCityOfMirrorsNearMyHouse
spotify: https://open.spotify.com/album/4FKocR65ABCl04K4QaDXil?si=x-RHZPehTs-nf9-_nED9_w
apple: https://music.apple.com/gb/album/my-neighbor-who-lives-in-the-city-of-mirrors-near-my-house/1547957873
tidal: https://tidal.com/browse/album/168724494
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC BY 3.0
download: https://drive.google.com/drive/folders/1ykDDNlagfUB2viDM3RbV8Z4F-pevdtno
---

From September to October 2015 I was the recipient of a Gasworks Fellowship residency with Britto Arts Trust, Dhaka, Bangladesh.

My initial thoughts turned to the noise-scape and I began, somewhat fruitlessly, to measure SPL levels around the city with the intention of finding "the quietest spot in Dhaka".

I ran listening groups with local artists which allowed me to share critical texts and sounds that have been formative in my artistic development and to interrogate my understandings in such a collective space.

Alongside this work I began recording spontaneous sonic improvisations with various materials to hand, local instruments, items bought off of the streets, found objects and environmental recordings/interventions along the way.

During my residency the political situation became difficult and the complexities of Bangladesh's political history became more apparent. After the murders of foreign NGO workers it was deemed best to leave early. This has been somewhat traumatic with feelings of guilt. Violence and attacks sporadically continue though and recognising addresses and buildings in news bulletins brings it all closer to home.

This package contains 2 books and a 5 audio tracks.

The first book is a combination of political history, journal, photos and cut-ups made in an attempt to make sense of the narratives running throughout these experiences.

The second book is a journal of my attempts to map the noise-scape. It contains the SPL readings and listening notes I took in various locations, my observations on this process, CD liner notes and references.

The 5 audio tracks were recorded and mixed whilst in Dhaka.

The work has now been privately published in an edition of 80 (see [http://cargocollective.com/mroystonward](http://cargocollective.com/mroystonward)).

This is a free/donation digital edition intended as archive and alternate distribution.
