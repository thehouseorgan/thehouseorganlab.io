---
layout: post
title: "Stop Stroking Your Chin!"
date: 2019-04-06
artist: "Jehova's Fitness"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=1085003397/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/stopstrokingyourchin_cover_375px.jpeg
bandcamp: http://thehouseorgan.bandcamp.com/album/stop-stroking-your-chin
resonate: https://beta.stream.resonate.coop/artist/9487/release/stop-stroking-your-chin
archive:
spotify: https://open.spotify.com/album/3SWLEwCdgw7rqKMZXJpTx0?si=lujQF_eISCSaOMXq_Os1Zg
apple: https://music.apple.com/gb/album/stop-stroking-your-chin/1530785739
tidal: https://tidal.com/browse/album/154520459
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC-BY
download: https://drive.google.com/drive/folders/1rfdp82svFG1E2B9t4s7xVcr0AAt3laDy
---
this is a mini-album (which is what you're meant to call an album that is shorter than a normal album) by jehova’s fitness, a noise rock band from berlin. that means we (mathew johnson and murray royston-ward who are writing this about our band) play songs but they’re a bit messy and chaotic at times. we also improvise a bit with our songs which means they’re made up as we go along so sometimes it’s a bit shit but we edited it all so most of the shit bits have been taken out so it's more fun. we recorded at the funkhaus in berlin because mathew works there and it didn't cost us any money. when murray sings it’s not really singing, it’s more like whining, straining, and choking, what phil minton calls “mucking about with your mouth”, but we think it’s good and also a bit funny too. mathew is a really good guitar player but to make it sound more exciting he’ll play lots of really fast or out of tune things so you’re not quite sure what’s coming next. we'll be going on a short tour in the summer so come and watch us if we're in your town, or you feel like travelling to another town.
