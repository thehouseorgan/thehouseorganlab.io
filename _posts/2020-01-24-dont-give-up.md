---
layout: post
title: "Don't Give Up"
date: 2020-01-24
artist: "The Sons of David Ginola"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=589627206/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/dont-give-up_375px-min.jpg
bandcamp: https://thehouseorgan.bandcamp.com/album/dont-give-up
archive:
resonate: https://beta.stream.resonate.coop/artist/6491/release/dont-give-up
spotify: https://open.spotify.com/album/2f0avBXxxKQFz9qFghhEIp
tidal: https://tidal.com/browse/album/132712292
apple: https://music.apple.com/gb/album/dont-give-up-live-in-sheffield-2019-07-20/1500717203
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC-0
download: https://drive.google.com/drive/folders/1wBSRBQYVGX74j30SoUKHfinbMYf624wz
---

2019-07-20 - Live in Sheffield

For The Sons of David Ginola's first live outing we attempted to honour the video for Peter Gabriel and Kate Bush's 'Don't Give Up'. We animated an eclipse and shared a tender embrace.

Working with the volume of a PA, everything sounded a little more raucous than the quiet domestic recordings we're used to. Regardless, there's still our characteristic pressure cooker stasis, crackles, creaks, and hisses.

Slightly edited and minimally mixed/mastered at home.

As always, it is released without copyright (CC0) http://creativecommons.org/publicdomain/zero/1.0/
