---
layout: post
title: "THO: Live Archive"
date: 2020-05-07
artist: "Various Artists"
description: ""
category:
tags: []
embed: "https://bandcamp.com/EmbeddedPlayer/album=2329052662/size=large/bgcol=ffffff/linkcol=9b9b9b/tracklist=false/transparent=true/"
image: /covers/tho-live-archive_375px-min.jpg
bandcamp: https://thehouseorgan.bandcamp.com/album/tho-live-archive
archive:
resonate:
spotify:
tidal:
apple:
cc-link: https://creativecommons.org/licenses/by/3.0/
cc: CC-BY
download: https://drive.google.com/drive/folders/1vPOcscMsFBFt6gHWlv7_QRRtxwGxkqRK
---

This is a 'living' archive of shows put on by The House Organ. It will be updated and added to on an ongoing basis rather than any one official, or final, release. The updating will of course be on hiatus during 2020, due to Covid-19 pandemic, but hopes to resume in the future.

So far then, these are recordings from the following shows:

- 2019-10-06: LuxuryMollusc / Nacht Und Nebel / Blackcloudsummoner @Jabbarwocky (see [FB event](https://www.facebook.com/events/2345594775531239))

*Unfortunately Blackcloudsummoner's set didn't record (my fault, didn't press the button twice) hence its absence.

- 2019-12-09: Kazehito Seki / SW1n-Hunter / Giblet Gusset @Access Space (see [FB event](https://www.facebook.com/events/415281119353856))

All performances are of course credited to the artists who have kindly allowed these to be released a) for free/PWYL, b) with permissive Creative Commons licensing, and c) also via p2p friendly methods (i.e. torrents).

Recorded and minimally edited, mixed, and mastered by myself (Murray Royston-Ward)
