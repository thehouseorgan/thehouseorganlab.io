---
layout: page
title: About
permalink: /about/
image: /images/tho.jpeg
---
The House Organ is a DIY label for self-published music.

## Streaming

Originally, I avoided streaming, instead focusing upon crypto and other alternatives. A lot of the p2p/crypto stuff is interesting but technical and it's tricky to navigate around the blockchain snake oil.

Streaming is terrible for artists but hey, if it's where people are listening, I've been making my music accessible to them.

If you have the choice, there are more artist friendly alternatives. [Resonate](https://resonate.is/) is a co-op streaming platform. [Ampled](https://www.ampled.com/) is a co-op patronage site (which might start featuring here soon). Buying on [Bandcamp](https://bandcamp.com/) supports artists directly.

## Pricing

I quietly changed the pricing on Bandcamp recently. There's a long history amongst my peers for pay-what-you-like pricing. I've decided to drop this. I don't like it. I think there's an unsustainable tendency for people not to get paid for their work. Releasing music and playing shows should be valued and whilst I don't see this as a commercial endeavour there's a basic math of affordability and sustainability and album/door prices should reflect that. I could say much more but I won't, it's my take and I'm sticking to it.

## Free

Despite what I've said above, PWYL and NOTAFLOF are prevalent for a reason. Scarce and precarious employment, high costs of living, and genuine attempts at accessibility.

Whilst I think that people being paid for their work actually helps with these issues in the long-term, in the short-term people still need a break.

Whilst you can't download from Bandcamp for free anymore, there will always be a free download link on this website. Currently this is a direct browser download  using [Keybase](https://keybase.io/) (though this might change in the future) and in some cases via [archive.org](https://archive.org/).

<img src="../images/tho.jpeg" />
