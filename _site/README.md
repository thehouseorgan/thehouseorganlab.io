# The House Organ

A homespun attempt at DIY digital distribution utilising p2p and crypto.

Feel free to re-use any code you find useful.

## Issues

Text overlays on the front page don't play nice with long titles.

Use of js for modals etc. obviously doesn't play nice with 'noscript'. This is a concern as also hosting over tor where security settings may prevent site from loading correctly. Either need to replace js with css or redesign layout.

<s>js qr code generator has above issues and also doesn't play nice with very long urls (i.e. BTSync addresses for long titled folders).</s> Fixed, replaced with static images.
